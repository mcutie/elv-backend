
function entorno() {
    var envJSON = require('./vars.json');
    var node_env = process.env.NODE_ENV || 'production';
    if (envJSON[node_env] == undefined) node_env = 'production';
    return envJSON[node_env];
}

module.exports = {
    entorno: entorno,
    
};
