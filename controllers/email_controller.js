const mailer = require('../helpers/mailer');
const captcha = require('../helpers/captcha_verifier');
const request = require('request');
const config = require('../config');

function EmailController() {

    this.sendEmail = async function (req, res) {
        try {
            console.log('Recibo :', req.body);
            let result = await captcha.verifyCaptcha(req);            
            console.log('verificacion captcha :', result);
            if (result) {
                let response = await mailer.wrapedSendMail(req.body);
                console.log('resultado del envio :', response);
                response ? res.json({ msg: 'Email enviado correctamente', error: false})
                    : res.json({ msg: 'Error al enviar el email', error: true})

            } else
                res.json({ msg: 'Error de validacion de captcha',error: true })

        } catch (error) {
            console.log(error)
            res.json({ msg: 'Solicitud incorrecta', error: true});
        }
    };
}
module.exports = new EmailController();


