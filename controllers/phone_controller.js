const phoneCaller = require('../helpers/phoneCaller');
const captcha = require('../helpers/captcha_verifier');
const config = require('../config');

function PhoneController() {

    this.phoneCall = async function (req, res) {
        try {
            console.log('Recibo :', req.body);
            let result = await captcha.verifyCaptcha(req);
            console.log('verificacion captcha :', result);
            if (result) {
                let response = await phoneCaller.caller(req.body.phone);
                console.log('respuesta del phone server :', response);
                response ? res.json({ msg: 'Solicitud recibida, en instantes se le llamara', error: false})
                    : res.json({ msg: 'Error del servidor de llamadas', error: true})

            } else
                res.json({ msg: 'Error de validacion de captcha',error: true })

        } catch (error) {
            console.log(error)
            res.json({ msg: 'Solicitud incorrecta', error: true});
        }
    };
}
module.exports = new PhoneController();


