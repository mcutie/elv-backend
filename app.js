//WE IMPORT THE PACKAGES
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var mailer = require('./helpers/mailer');
var config = require("./config");
var app = express();


app.use(cors());
app.use(bodyParser.json({ limit: '10mb', extended: false }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: false }));


//IMPORT ROUTES

var emailRoute = require('./routes/email_route');
var phoneRoute = require('./routes/phone_route');
app.use('/api/sendEmail', emailRoute);
app.use('/api/phoneCall', phoneRoute);

//Start the server
const server = app.listen(config.entorno().PORT);
console.log('Running in port: ',config.entorno().PORT);







