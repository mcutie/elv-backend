var express = require('express');
var routes = express.Router();

var Controller = require('../controllers/email_controller');
                                                  
      
routes.post("/", Controller.sendEmail);  
       

module.exports = routes;