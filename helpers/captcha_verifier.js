request = require('request');
config = require('../config');

function CaptchaVerifier() {

  this.verifyCaptcha = async function (req) {
    return new Promise((resolve, reject) => {
      if (req.body['g-recaptcha-response'] === undefined ||
        req.body['g-recaptcha-response'] === '' ||
        req.body['g-recaptcha-response'] === null) {
        console.log('No recibo captcha')
        resolve(false);
      }
      const secretKey = config.entorno().recaptchSecretKey;
      console.log(secretKey)
      console.log(req.connection.remoteAddress)
      const verificationURL = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body['g-recaptcha-response'] + "&remoteip=" + req.connection.remoteAddress;
      request(verificationURL, function (error, response, body) {
        body = JSON.parse(body);
        if (body.success !== undefined && !body.success) {
          console.log('body.success :', body.success)
          console.log('captcha incorrecta')
          resolve(false);
        }
        console.log('captcha correcta')
        console.log('body.success :', body.success)
        resolve(true);
      });
    })

  }
}
module.exports = new CaptchaVerifier();