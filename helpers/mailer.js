
var config = require('../config');
var nodemailer = require('nodemailer');
var htmlLogo = require('./logo');

function EmailSender() {

	let host = config.entorno().emailHost;
	let port = config.entorno().emailPort;
	let user = config.entorno().emailUser;
	let pass = config.entorno().emailPassword;
	let logo = htmlLogo.logo;	
	let from = `ELV Soluciones y Reformas <${user}>`;
	let to = `raul.marquez@elvsoluciones.com,  administracion@elvsoluciones.com, manuelcutie@gmail.com`;
	//let to = `manuelcutie@gmail.com`;

	var transporter = nodemailer.createTransport({

		type: "SMTP",
		host,
		secure: true,
		port,
		auth: {
			user,
			pass
		}
	});


	this.wrapedSendMail = async function (data) {
		var mailOptions = {
			from,
			to,
			subject: 'ELV Sitio Web : Notificación de nuevo mensaje.\n',
			html: `
			<html>
			<head>
			  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			  <title>ELV Soluciones y Reformas</title>
			  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
			</head>		  
			<body>			
			${logo}<hr>			
			<p>
			<h4>El cliente ${data.name} con teléfono ${data.phone} y correo electrónico ${data.email}, ha escrito el siguiente mensaje:</h4>
			</p>			
			<p>
			<h4>${data.message}</h4>
			</p>
			</div>			  
			</body>		  
		  </html>`
		};

		return new Promise((resolve, reject) => {
			transporter.sendMail(mailOptions, function (error, info) {
				if (error) {
					console.log("error is " + error);
					resolve(false); // or use rejcet(false) but then you will have to handle errors
				}
				else {
					console.log('Email sent: ' + info.response);
					resolve(true);
				}
			});
		})
	}
}
module.exports = new EmailSender();



