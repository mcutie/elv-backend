
const config = require('../config');
const axios = require('axios');

function PhoneCaller() {

	this.caller = async function (phone) {
		let url = config.entorno().phoneServer;
	let data = {
		data: {phone: phone}
	}

	axios.post(url, data)
	  .then(function (response) {
		console.log('response :',response);
		return response
	  })
	  .catch(function (error) {
		console.log('error :',error);
		return error
	  });

	}

	
	
}
module.exports = new PhoneCaller();



